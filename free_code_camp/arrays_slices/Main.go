package main

import "fmt"

func main() {

	// Arrays: 
	//	- Creation
	
	grade1 := 97
	grade2 := 85
	grade3 := 93

	fmt.Printf("Grades: %v, %v, %v", grade1, grade2, grade3)

	//grades := [3]int{grade1, grade2, grade3} -> Create ana array with 3 position for int and initialize it
	grades := [...]int{97, 85, 93} // create an array bigthe size of elements in the arguments
	fmt.Printf("Grades: %v", grades)


	var students[3]string // create an array empity
	students[0] = 'Lisa'
	students[2] = 'Lucas'

	fmt.Printf('Students: %v, %v\n', students, students[0])
	fmt.Printf('Tamanho: %v', len(students))

	var identityMatrix [3][3]int{ [3]int{1, 0, 0}, [3]int{0, 1, 0}, [3]int{0, 0, 1} }
	fmt.Prinln(identityMatrix)

	// in Go, arrays are actualy values! 

	a := [...]int{1, 2, 3, 4}

	b := a
	b[1] = 5
	fmt.Println(a) // -> [1 2 3 4]
	fmt.Println(b) // -> [1 5 3 4]

	//passing arrays in functions as paramaters could be a little "lazy"
	c := &a //c points to a (like pointers in C)
	c[1] = 5
	fmt.Println(a) // -> [1 5:w 3 4]
	fmt.Println(c) // -> [1 5 3 4]


	// Slices:
	// everything we can do with an array, we can do with an slice. Slices are based on array, but with more power
	// - Creation
	_a := []int{1, 2, 3}
	_b := _a // slices are naturally copyting the addres of variable, not the content

	b[1] = 5

	fmt.Printf("Lenght: %v\n", len(_a))
	fmt.Printf("Capacity: %v\n", cap(_a))

	slice1 := []int{1, 2, 3, 4, 5, 6, 7,8, 9, 10}
	slice2 := slice1[:] // with all elements
	slice3 := slice1[3:] // slice from the 4th element to the end
	slice4 := slice1[:6] // slice from the beginning to the 7th element
	slice5 := slice1[3:6] //slice from the 4th, 5th and 6th elements

	fmt.Printf("%v, %v, %v, %v, %v", slice1, slice2, slice3, slice4, slice5)

	new_slice := make([]int, 3, 100) // create an slice of integers with size 3, and capacity of 100

	fmt.Println(new_slice) // [0 0 0]
	fmt.Printf("Lenght: %v\n", len(new_slice)) // 3
	fmt.Printf("Capacity: %v\n", cap(neiw_slice)) // 100

	// slices dont have a fixed value, arrays hav

	ap_slice := []int{}

	fmt.Println(ap_slice) // [ ]
	fmt.Printf("Lenght: %v\n", len(ap_slice)) // 0
	fmt.Printf("Capacity: %v\n", cap(ap_slice)) // 0

	ap_slice = append(ap_slice, 1, 2, 3) // after the first parameter, all items will be appended on slice

	fmt.Println(ap_slice) // [ 1 ]
	fmt.Printf("Lenght: %v\n", len(ap_slice)) // 1
	fmt.Printf("Capacity: %v\n", cap(ap_slice)) // 2 (always double the capacity to avoid seg fault

	// concatenate two or more slices by using the spread operator (...)
	ap_slice = append(ap_slice, []int{42, 43, 44, 45}...)
	
	fmt.Println(ap_slice) // [ 1 ]
	fmt.Printf("Lenght: %v\n", len(ap_slice)) // 1
	fmt.Printf("Capacity: %v\n", cap(ap_slice)) // 2 (always double the capacity to avoid seg fault


	// Stack operations
	_stack := []int{1, 2, 3, 4, 5} // -> stack
	_stack = _stack[1:] // -> pop out the first element in stack
	_stack = _stack[:len(_stack) - 1] // -> pop out the first element in stack
	_stack = append(_stack, 6) // added 6 to the tail of the stack

	// removig an item in the middle of stack
	_stack = append(_stack[:2], _stack[3:]...)

	fmt.Printf(_stack)



	// Slices:
	// - Built-in Functions

	// Slices:
	// - Working with slices


}
