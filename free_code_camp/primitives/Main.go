package main

import "fmt"

func main() {
	// Boolean
	// Numeric Types
	// - Integers
	// - Floating Point
	// - Complex Numbers
	// Text types

	// Boolean:
	var n bool = true
	fmt.Printf("%v, %T", n, n)

	n = false
	fmt.Printf("%v, %T", n, n)

	m := 1 == 1
	o := 1 == 2
	// All variables initiate with 0, when bool, starts with false

	var new_var bool

	fmt.Printf("%v, %T", m, m)
	fmt.Printf("%v, %T", o, o)
	fmt.Printf("%v, %T", new_var, new_var)

	// Numeric types
	// Integers, at least 32 bit, signed or unsigned
	num := 42
	fmt.Printf("%v, %T", num, num)

	a := 10
	b := 3

	fmt.Println(a + b)
	fmt.Println(a - b)
	fmt.Println(a * b)
	fmt.Println(a / b)
	fmt.Println(a % b)

	// But go cant sum int + int8, it's needed to convert
	// int + int(int8))

	// Bits operator
	fmt.Println(a & b)
	fmt.Println(a | b)
	fmt.Println(a ^ b)
	fmt.Println(a &^ b)

	a := 8
	fmt.Println(a << 3)
	fmt.Println(a >> 3)

	// Float Points
	// Comples
	var n complex64 = 1 + 2i
	var _n complex128 = complex(1, 2)
	fmt.Printf("%v, %T", real(n), real(n))
	fmt.Printf("%v, %T", imag(n), imag(n))
	fmt.Printf("%v, %T", _n, n)

	// Strings area alias to bytes!
	s := "This is a string"
	fmt.Printf("%v, %T", s, s)               // prints the string and the type, but
	fmt.Printf("%v, %T", s[2], s[2])         // will print 104 and int8, the byte representation of the letter 'i', third char in s, and
	fmt.Printf("%v, %T", string(s[2]), s[2]) // will print the byte value 104 in char representation

	s[2] = "u" // it's an erros, cuz we're atributing as char into a byte

	s2 := "New String!"

	fmt.Printf("%v, %T", s+s2, s+s2) // -> Concatenates the String

	// We can create a slice of byte,
	b := []bytes(s)
	fmt.Printf("%v, %T", b, b) // -> Converts the string to byte representatio

	/*
		While sending data as text to another application, it easier to use string as charecters., but if your dealing with ana rchieve
		send this to another application will be easier with the string as bytes!
	*/

	// ROOM, utf32 character, strings deal with utf8, but it's not too used cuz string contemplates rooms field

	//r := 'a' // single quotes for chars (room)
	var r rune = 'a'
	fmt.Printf("%v, %T", r, r) // -> and its prints 97, int32

	// room === int32
}
