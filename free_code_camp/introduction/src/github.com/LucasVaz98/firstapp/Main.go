package main

import (
	"fmt"
	"strconv"
)

var (
	actorName  string = "Elisabeth Sladden"
	companion  string = "Sarah Jane Smith"
	doctNumber int    = 3
	season     int    = 11
)

var (
	counter int = 0
)

// with capital letter, any file in any package can access this variable
var I int = 42

func main() {
	// this variable is closed to this package
	var i int = 27

	//var theURL string = "https://google.com"
	//var HTTP string = "https://google.com"

	var j float32
	j = float32(i)
	var k string
	k = strconv.Itoa(i)
	fmt.Printf("%v, %T\n%v, %T\n", i, i, j, j)
	fmt.Printf("%v, %T", k, k)
}
