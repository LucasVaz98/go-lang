package main

import "fmt"

const a int16 = 17


// Enumerated const
const _d = iota


// iota works like a counter to create enumerated consts
const (
	e = iota
	f = iota
	g = iota
)
// but the compiler can "complete", if you don't specify f and g, like so
/*
const (
	e = iota
	f
	g
)
*/
const (
	e2 = iota
)

const (
	//errorSpecialist = iota if you want to set an error if the value isn't assigned
	_ = iota + 5 // but this way the compiler throws away the first value, don't store this value in memory
	catSpecialist
	dogSpecialist
	humanSpecialist
)

// in Go we can't use power in constants, cuz power is function from math package
// So we use bitshifting, to power to the level of 2
const (
	_ = iota
	KB = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YB
)

const (
	idAdmin = 1 << iota
	isHeadQuarters
	canSeeFinancials

	canSeeAfrica
	canSeeAsia
	canSeeEurope
	canSeeNorthAmerica
	canSeeSouthAmerica
)

func main() {
	//If we name our constats like
	//const MY_CONST
	// MY_CONST (with the first letter being a capital letter)
	// would be exported, but we do not always want that, so we name
	// constants like variables

	// When not exporting
	const myConst 
	// When exporting
	const MyConst

	const anotherConst int = 42
	fmt.Printf("%v, %T\n", anotherConst, anotherConst)

	// const are imutable
	// are set in compile time, not run time
	// wrong:
	const a int = math.sin(1.57)
	// right:
	const b int = 2

	// thy can be shadowed
	const a int = 14

	fmt.Printf("%v, %T\n", a, a) // -> prints 14, int

	// can add constants to a variable, this sums returns a variable,
	// but need to be in the same type

	//wrong
	const b int = 1
	var c int16 = 3
	fmt.Printf("%v, %T", b+c, b+c)

	//right
	const _b int = 1
	var _c int16 = 3
	fmt.Printf("%v, %T", _b+_c, _b+_c)

	const d = 42
	// this way the compiler is inferring the const type using the valeu, 
	// so we can do
	fmt.Printf("%v, %T", _b+d, _b+d)
	// and will work

	fmt.Printf("%v, %T", _d, _d) //-> this prints 0, int 

	fmt.Printf("%v, %T", e) // this prints 0, int
	fmt.Printf("%v, %T", f) // this prints 1, int
	fmt.Printf("%v, %T", g) // this prints 2, int

	// but iota reset in another const block
	fmt.Printf("%v, %T", e2, e2)

	var specialistType int = catSpecialist
	fmt.Printf("%v", specialistType) // -> this prints 1 (don't considering the sum with iota
	fmt.Printf("%v", specialistType == catSpecialist)

	fmt.Printf("%v", catSpecialist)


	fileSize := 4000000000
	fmt.Printf("%.2fGB", fileSize/GB) // this prints -> 3.73GB


	var roles byte = isAdmin | canSeeFinancials | canSeeEurope
	fmt.Print("%b\n", roles) // this prints -> 00100101
	fmt.Print("Is admin? %v", isAdmin & roles == isAdmin)


}

